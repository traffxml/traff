Class|Type|Text_Q0|Text_Q|Quantifier|Speed|Since|Notes
-|-|-|-|-|-|-|-
ACTIVITY|(null)|||||0.8|
ACTIVITY|ACTIVITY_ATHLETICS_MEETING|athletics meeting||||0.8|
ACTIVITY|ACTIVITY_AUTOMOBILE_RACE|automobile race||||0.8|
ACTIVITY|ACTIVITY_BALL_GAME|ball game||||0.8|
ACTIVITY|ACTIVITY_BASEBALL_GAME|baseball game||||0.8|
ACTIVITY|ACTIVITY_BASKETBALL_GAME|basketball game||||0.8|
ACTIVITY|ACTIVITY_BOAT_RACE|boat race||||0.8|
ACTIVITY|ACTIVITY_BOXING_TOURNAMENT|boxing tournament||||0.8|
ACTIVITY|ACTIVITY_BULL_FIGHT|bull fight||||0.8|
ACTIVITY|ACTIVITY_CEREMONIAL_EVENT|ceremonial event||||0.8|
ACTIVITY|ACTIVITY_CONCERT|concert||||0.8|
ACTIVITY|ACTIVITY_CRICKET_MATCH|cricket match||||0.8|
ACTIVITY|ACTIVITY_CROWD|crowd||||0.8|
ACTIVITY|ACTIVITY_CYCLE_RACE|cycle race||||0.8|
ACTIVITY|ACTIVITY_DEMONSTRATION|demonstration||||0.8|
ACTIVITY|ACTIVITY_EMERGENCY_TRAINING|emergency training in progress|||||
ACTIVITY|ACTIVITY_EVENT|event||||0.8|
ACTIVITY|ACTIVITY_EVENT_ENDED|information about major event no longer valid|||||
ACTIVITY|ACTIVITY_EXHIBITION|exhibition||||0.8|
ACTIVITY|ACTIVITY_FAIR|fair||||0.8|
ACTIVITY|ACTIVITY_FESTIVAL|festival||||0.8|
ACTIVITY|ACTIVITY_FILM_TV_PRODUCTION|film or TV production||||0.8|
ACTIVITY|ACTIVITY_FUNFAIR|funfair||||0.8|
ACTIVITY|ACTIVITY_GOLF_TOURNAMENT|golf tournament||||0.8|
ACTIVITY|ACTIVITY_HOCKEY_GAME|hockey game||||0.8|
ACTIVITY|ACTIVITY_INTERNATIONAL_SPORTS_EVENT|international sports meeting||||0.8|
ACTIVITY|ACTIVITY_MAJOR_EVENT|major event||||0.8|
ACTIVITY|ACTIVITY_MARATHON|marathon||||0.8|
ACTIVITY|ACTIVITY_MARCH|march||||0.8|
ACTIVITY|ACTIVITY_MARKET|market||||0.8|
ACTIVITY|ACTIVITY_MATCH|match||||0.8|
ACTIVITY|ACTIVITY_MILITARY_TRAINING|military training in progress|||||
ACTIVITY|ACTIVITY_PARADE|parade||||0.8|
ACTIVITY|ACTIVITY_PROCESSION|procession||||0.8|
ACTIVITY|ACTIVITY_PROTEST|protest||||0.8|
ACTIVITY|ACTIVITY_PUBLIC_DISTURBANCE|public disturbance||||0.8|
ACTIVITY|ACTIVITY_RACE_MEETING|race meeting||||0.8|
ACTIVITY|ACTIVITY_RUGBY_MATCH|rugby match||||0.8|
ACTIVITY|ACTIVITY_SEVERAL_EVENTS|several major events||||0.8|
ACTIVITY|ACTIVITY_SHOW|show||||0.8|
ACTIVITY|ACTIVITY_SHOW_JUMPING|show jumping||||0.8|
ACTIVITY|ACTIVITY_SOCCER_MATCH|football match||||0.8|
ACTIVITY|ACTIVITY_SPORTS_EVENT|sports event meeting||||0.8|
ACTIVITY|ACTIVITY_SPORTS_TRAFFIC_CLEARED|sports traffic cleared|||||
ACTIVITY|ACTIVITY_STATE_OCCASION|state occasion||||0.8|
ACTIVITY|ACTIVITY_STRIKE|strike||||0.8|
ACTIVITY|ACTIVITY_TENNIS_TOURNAMENT|tennis tournament||||0.8|
ACTIVITY|ACTIVITY_TOURNAMENT|tournament||||0.8|
ACTIVITY|ACTIVITY_TRADE_FAIR|trade fair||||0.8|
ACTIVITY|ACTIVITY_TRAFFIC_DISRUPTION_CLEARED|traffic disruption cleared|||||
ACTIVITY|ACTIVITY_VEHICLE_DEMONSTRATION|demonstration by vehicles|||||
ACTIVITY|ACTIVITY_WATER_SPORTS_MEETING|water sports meeting||||0.8|
ACTIVITY|ACTIVITY_WINTER_SPORTS_MEETING|winter sports meeting||||0.8|
AUDIO_BROADCAST|(null)|||||Reserved|
AUDIO_BROADCAST|AUDIO_BROADCAST_DETAILED_INFORMATION_ANNOUNCE|detailed information will be given on normal program broadcasts|detailed information will be given at $Q on normal program broadcasts|q_time|||
AUDIO_BROADCAST|AUDIO_BROADCAST_REFERENCES_INVALIDATED|reference to audio programs no longer valid|||||
AUDIO_BROADCAST|AUDIO_BROADCAST_SWITCH_TO_AM|switch on your car radio|switch your car radio to $Q|q_frequency|||
AUDIO_BROADCAST|AUDIO_BROADCAST_SWITCH_TO_FM|switch on your car radio|switch your car radio to $Q|q_frequency|||
AUDIO_BROADCAST|AUDIO_BROADCAST_URGENT_INFORMATION_ANNOUNCE|urgent information will be given on normal program broadcasts|urgent information will be given at $Q on normal program broadcasts|q_time|||
AUTHORITY|(null)|||||0.8|
AUTHORITY|AUTHORITY_CHECKPOINT|checkpoint||||0.8|
AUTHORITY|AUTHORITY_INVESTIGATION|investigation or juridical reconstruction||||0.8|
AUTHORITY|AUTHORITY_SPEED_CHECKS|speed checks in operation||||0.8|
AUTHORITY|AUTHORITY_VIP_PASSING|passage of important personality||||0.8|
AUTHORITY|AUTHORITY_WINTER_EQUIPMENT_CHECKS|winter equipment checks||||0.8|
CARPOOL|(null)|||||0.8|
CARPOOL|CARPOOL_LANE_BLOCKED|carpool lane blocked|$Q person carpool lane blocked|q_int|||
CARPOOL|CARPOOL_LANE_CLOSED|carpool lane closed|$Q person carpool lane closed|q_int|||
CARPOOL|CARPOOL_LANE_OPERATIONAL|carpool lane in operation|$Q person carpool lane in operation|q_int||0.8|
CARPOOL|CARPOOL_RESTRICTION_CHANGED|carpool restrictions changed|carpool restrictions changed to $Q persons per vehicle|q_int|||
CARPOOL|CARPOOL_RESTRICTION_LIFTED|carpool restrictions lifted|$Q person carpool restrictions lifted|q_int|||
CARPOOL|CARPOOL_USE_BUS_LANE|bus lane available for carpools|bus lane available for carpools with at least $Q occupants|q_int|||
CONGESTION|(null)|||||0.1|
CONGESTION|CONGESTION_CLEARED|traffic congestion cleared||||0.1|
CONGESTION|CONGESTION_FORECAST_WITHDRAWN|traffic congestion forecast withdrawn||||0.1|
CONGESTION|CONGESTION_HEAVY_TRAFFIC|heavy traffic|heavy traffic with average speeds $Q||average|0.1|
CONGESTION|CONGESTION_LONG_QUEUE|long queues|long queues with average speeds $Q||average|0.1|
CONGESTION|CONGESTION_NONE|no problems to report||||0.1|
CONGESTION|CONGESTION_NORMAL_TRAFFIC|traffic has returned to normal||||0.1|
CONGESTION|CONGESTION_QUEUE|queuing traffic|queuing traffic with average speeds $Q||average|0.1|
CONGESTION|CONGESTION_QUEUE_LIKELY|danger of queuing traffic|danger of queuing traffic with average speeds $Q||average|0.1|
CONGESTION|CONGESTION_SLOW_TRAFFIC|slow traffic|slow traffic with average speeds $Q||average|0.1|
CONGESTION|CONGESTION_STATIONARY_TRAFFIC|stationary traffic||||0.1|
CONGESTION|CONGESTION_STATIONARY_TRAFFIC_LIKELY|danger of stationary traffic||||0.1|
CONGESTION|CONGESTION_TRAFFIC_BUILDING_UP|traffic building up|traffic building up with average speeds $Q||average|0.1|
CONGESTION|CONGESTION_TRAFFIC_CONGESTION|traffic congestion|traffic congestion with average speeds $Q||average|0.1|
CONGESTION|CONGESTION_TRAFFIC_EASING|traffic easing||||0.1|
CONGESTION|CONGESTION_TRAFFIC_FLOWING_FREELY|traffic flowing freely|traffic flowing freely with average speeds $Q||average|0.1|
CONGESTION|CONGESTION_TRAFFIC_HEAVIER_THAN_NORMAL|traffic heavier than normal|traffic heavier than normal with average speeds $Q||average|0.1|
CONGESTION|CONGESTION_TRAFFIC_LIGHTER_THAN_NORMAL|traffic lighter than normal|traffic lighter than normal with average speeds $Q||average|0.1|
CONGESTION|CONGESTION_TRAFFIC_MUCH_HEAVIER_THAN_NORMAL|traffic very much heavier than normal|traffic very much heavier than normal with average speeds $Q||average|0.1|
CONGESTION|CONGESTION_TRAFFIC_PROBLEM|traffic problem||||0.1|
CONSTRUCTION|(null)|||||0.8|
CONSTRUCTION|CONSTRUCTION_BLASTING|blasting work|$Q sections of blasting work|q_int||0.8|
CONSTRUCTION|CONSTRUCTION_BRIDGE_DEMOLITION|bridge demolition work|bridge demolition work at $Q bridges|q_int|||
CONSTRUCTION|CONSTRUCTION_BRIDGE_MAINTENANCE|bridge maintenance work|bridge maintenance work at $Q bridges|q_int||0.8|
CONSTRUCTION|CONSTRUCTION_BURIED_CABLES|work on buried cables|$Q sets of work on buried cables|q_int||0.8|
CONSTRUCTION|CONSTRUCTION_BURIED_SERVICES|work on buried services|$Q sets of work on buried services|q_int||0.8|
CONSTRUCTION|CONSTRUCTION_CENTRAL_RESERVATION_WORK|central reservation work|$Q sets of central reservation work|q_int|||
CONSTRUCTION|CONSTRUCTION_CONSTRUCTION_WORK|construction work|$Q sets of construction work|q_int||0.8|
CONSTRUCTION|CONSTRUCTION_DEMOLITION_WORK|demolition work|$Q sets of demolition work|q_int||0.8|
CONSTRUCTION|CONSTRUCTION_GAS_MAINS|gas main work|$Q sets of gas main work|q_int||0.8|
CONSTRUCTION|CONSTRUCTION_LONGTERM_ROADWORKS|long-term roadworks|$Q sets of long-term roadworks|q_int||0.8|
CONSTRUCTION|CONSTRUCTION_MAINTENANCE|maintenance work|$Q sets of maintenance work|q_int||0.8|
CONSTRUCTION|CONSTRUCTION_MAINTENANCE_CLEARED|maintenance work cleared|||||
CONSTRUCTION|CONSTRUCTION_MAJOR_ROADWORKS|major roadworks|$Q sets of major roadworks|q_int||0.8|
CONSTRUCTION|CONSTRUCTION_NEW_ROAD_LAYOUT|new road layout|||||
CONSTRUCTION|CONSTRUCTION_NEW_ROADWORKS_LAYOUT|new roadworks layout||||0.8|
CONSTRUCTION|CONSTRUCTION_REPAIR|repair work||||0.8|
CONSTRUCTION|CONSTRUCTION_RESURFACING_WORK|resurfacing work|$Q sections of resurfacing work|q_int||0.8|
CONSTRUCTION|CONSTRUCTION_ROAD_LAYOUT_UNCHANGED|road layout unchanged|||||
CONSTRUCTION|CONSTRUCTION_ROADWORK_CLEARANCE|roadwork clearance in progress||||0.8|
CONSTRUCTION|CONSTRUCTION_ROADWORKS|roadworks|$Q sets of roadworks|q_int||0.8|
CONSTRUCTION|CONSTRUCTION_ROADWORKS_CLEARED|roadworks cleared|||||
CONSTRUCTION|CONSTRUCTION_TEMP_TRAFFIC_LIGHT|temporary traffic lights|$Q sets of temporary traffic lights|q_int||0.8|
CONSTRUCTION|CONSTRUCTION_WATER_MAINS|water main work|$Q sets of water main work|q_int||0.8|
DELAY|(null)|||||0.1|
DELAY|DELAY_CLEARANCE|delays cleared||||0.1|
DELAY|DELAY_DELAY|delays|delays $Q|q_duration||0.1|
DELAY|DELAY_DELAY_POSSIBLE|delays possible|delays $Q possible|q_duration||0.1|
DELAY|DELAY_FORECAST_WITHDRAWN|delay forecast withdrawn||||0.1|
DELAY|DELAY_LONG_DELAY|long delays|long delays $Q|q_duration||0.1|
DELAY|DELAY_SEVERAL_HOURS|delays of several hours||||0.1|
DELAY|DELAY_UNCERTAIN_DURATION|delays of uncertain duration||||0.1|
DELAY|DELAY_VERY_LONG_DELAY|very long delays|very long delays $Q|q_duration||0.1|
ENVIRONMENT|(null)|||||0.8|
ENVIRONMENT|ENVIRONMENT_AIR_QUALITY_FAIR|air quality: fair||q_percent|||
ENVIRONMENT|ENVIRONMENT_AIR_QUALITY_GOOD|air quality: good||q_percent|||
ENVIRONMENT|ENVIRONMENT_AIR_QUALITY_IMPROVED|air quality improved|||||
ENVIRONMENT|ENVIRONMENT_AIR_QUALITY_POOR|air quality: poor||q_percent|||
ENVIRONMENT|ENVIRONMENT_AIR_QUALITY_VERY_POOR|air quality: very poor||q_percent|||
ENVIRONMENT|ENVIRONMENT_EXHAUST_POLLUTION|severe exhaust pollution||||0.8|
ENVIRONMENT|ENVIRONMENT_POLLEN_HIGH|pollen count: high||q_percent|||
ENVIRONMENT|ENVIRONMENT_POLLEN_LOW|pollen count: low||q_percent|||
ENVIRONMENT|ENVIRONMENT_POLLEN_MEDIUM|pollen count: medium||q_percent|||
ENVIRONMENT|ENVIRONMENT_SMOG|severe smog||||0.8|
EQUIPMENT_STATUS|(null)|||||0.8|
EQUIPMENT_STATUS|EQUIPMENT_STATUS_AUTOMATIC_PAYMENT_LANES_NOT_WORKING|automatic payment lanes not working|$Q automatic payment lanes not working|q_int|||
EQUIPMENT_STATUS|EQUIPMENT_STATUS_AUTOMATIC_TOLL_SYSTEM_NOT_WORKING|automatic toll system not working, pay manually||||0.8|
EQUIPMENT_STATUS|EQUIPMENT_STATUS_ELECTRONIC_SIGNS_REPAIRED|electronic signs repaired|||||
EQUIPMENT_STATUS|EQUIPMENT_STATUS_EMERGENCY_CALL_FACILITIES_RESTORED|emergency call facilities restored|||||
EQUIPMENT_STATUS|EQUIPMENT_STATUS_EMERGENCY_PHONE_NUMBER_NOT_WORKING|emergency telephone number not working|||||
EQUIPMENT_STATUS|EQUIPMENT_STATUS_EMERGENCY_PHONES_NOT_WORKING|emergency telephones not working||||0.8|
EQUIPMENT_STATUS|EQUIPMENT_STATUS_LANE_CONTROL_SIGNS_NOT_WORKING|lane control signs not working||||0.8|
EQUIPMENT_STATUS|EQUIPMENT_STATUS_LANE_CONTROL_SIGNS_OPERATING|lane control signs operating|||||
EQUIPMENT_STATUS|EQUIPMENT_STATUS_LANE_CONTROL_SIGNS_WORKING_INCORRECTLY|lane control signs working incorrectly||||0.8|
EQUIPMENT_STATUS|EQUIPMENT_STATUS_LEVEL_CROSSING_FAILURE|level crossing failure||||Deprecated|Use EQUIPMENT_STATUS_LEVEL_CROSSING_NOT WORKING instead
EQUIPMENT_STATUS|EQUIPMENT_STATUS_LEVEL_CROSSING_NOT_WORKING|level crossing failure||||0.8|
EQUIPMENT_STATUS|EQUIPMENT_STATUS_LEVEL_CROSSING_OK|level crossing now working normally|||||
EQUIPMENT_STATUS|EQUIPMENT_STATUS_MESSAGE_SIGNS_NOT_WORKING|variable message signs not working|||||
EQUIPMENT_STATUS|EQUIPMENT_STATUS_MESSAGE_SIGNS_OPERATING|variable message signs operating|||||
EQUIPMENT_STATUS|EQUIPMENT_STATUS_MESSAGE_SIGNS_WORKING_INCORRECTLY|variable message signs working incorrectly|||||
EQUIPMENT_STATUS|EQUIPMENT_STATUS_POWER_FAILURE|power failure|||||
EQUIPMENT_STATUS|EQUIPMENT_STATUS_RAMP_SIGNALS_NOT_WORKING|ramp control signals not working|$Q sets of ramp control signals not working|q_int||0.8|
EQUIPMENT_STATUS|EQUIPMENT_STATUS_RAMP_SIGNALS_WORKING_INCORRECTLY|ramp control signals working incorrectly|$Q sets of ramp control signals working incorrectly|q_int||0.8|
EQUIPMENT_STATUS|EQUIPMENT_STATUS_TEMPORARY_TRAFFIC_LIGHTS_NOT_WORKING|temporary traffic lights not working|$Q sets of temporary traffic lights not working|q_int||0.8|
EQUIPMENT_STATUS|EQUIPMENT_STATUS_TEMPORARY_TRAFFIC_LIGHTS_WORKING_INCORRECTLY|temporary traffic lights working incorrectly|$Q sets of temporary traffic lights working incorrectly|q_int||0.8|
EQUIPMENT_STATUS|EQUIPMENT_STATUS_TRAFFIC_LIGHTS_NOT_WORKING|traffic lights not working|$Q sets of traffic lights not working|q_int||0.8|
EQUIPMENT_STATUS|EQUIPMENT_STATUS_TRAFFIC_LIGHTS_WORKING_INCORRECTLY|traffic lights working incorrectly|$Q sets of traffic lights working incorrectly|q_int||0.8|
EQUIPMENT_STATUS|EQUIPMENT_STATUS_TRAFFIC_SIGNAL_CONTROL_NOT_WORKING|traffic signal control computer not working|||||
EQUIPMENT_STATUS|EQUIPMENT_STATUS_TRAFFIC_SIGNAL_TIMINGS_CHANGED|traffic signal timings changed|||||
EQUIPMENT_STATUS|EQUIPMENT_STATUS_TRAFFIC_SIGNALS_REPAIRED|traffic signals repaired|||||
EQUIPMENT_STATUS|EQUIPMENT_STATUS_TUNNEL_VENTILATION_NOT_WORKING|tunnel ventilation not working|||||
HAZARD|(null)|||||0.8|
HAZARD|HAZARD_ABNORMAL_LOAD|abnormal load(s)|$Q abnormal load(s)|q_int||0.8|
HAZARD|HAZARD_AIR_CRASH|air crash||||0.8|
HAZARD|HAZARD_ALMOST_IMPASSABLE|almost impassable||||0.8|
HAZARD|HAZARD_ALMOST_IMPASSABLE_ABOVE_ELEVATION|almost impassable|almost impassable above $Q hundred meters|q_int||Deprecated|Use HAZARD_ALMOST_IMPASSABLE, S_PLACE_HIGH_ALTITUDE, q_dimension instead
HAZARD|HAZARD_ANIMAL_HERDS_ON_ROAD|herds of animals on roadway||||0.8|
HAZARD|HAZARD_ANIMALS_ON_ROAD|animals on roadway||||0.8|
HAZARD|HAZARD_AQUAPLANING_LIKELY|danger of aquaplaning|||||
HAZARD|HAZARD_AVALANCHE|avalanches||||0.8|
HAZARD|HAZARD_AVALANCHE_RISK|avalanche risk|||||
HAZARD|HAZARD_BAD_ROAD|road surface in poor condition||||0.8|
HAZARD|HAZARD_BLACK_ICE|black ice||||0.8|
HAZARD|HAZARD_BLACK_ICE_ABOVE_ELEVATION|black ice|black ice above $Q hundred meters|q_int||Deprecated|Use HAZARD_BLACK_ICE, S_PLACE_HIGH_ALTITUDE, q_dimension instead
HAZARD|HAZARD_BLACK_ICE_LIKELY|danger of black ice||||0.8|
HAZARD|HAZARD_BLACK_ICE_LIKELY_ABOVE_ELEVATION|danger of black ice|danger of black ice above $Q hundred meters|q_int||Deprecated|Use HAZARD_BLACK_ICE_LIKELY, S_PLACE_HIGH_ALTITUDE, q_dimension instead
HAZARD|HAZARD_BLOCKED_BY_SNOW|road blocked by snow||||0.8|
HAZARD|HAZARD_BLOCKED_BY_SNOW_ABOVE_ELEVATION|road blocked by snow|road blocked by snow above $Q hundred meters|q_int||Deprecated|Use HAZARD_BLOCKED_BY_SNOW, S_PLACE_HIGH_ALTITUDE, q_dimension instead
HAZARD|HAZARD_BRIDGE_DAMAGE|bridge damage||||0.8|
HAZARD|HAZARD_BURST_PIPE|burst pipe(s)|$Q burst pipe(s)|q_int||0.8|
HAZARD|HAZARD_BURST_WATER_MAIN|burst water main||||0.8|
HAZARD|HAZARD_CHILDREN_ON_ROAD|children on roadway||||0.8|
HAZARD|HAZARD_COLLAPSED_SEWER|collapsed sewer(s)|$Q collapsed sewer(s)|q_int||0.8|
HAZARD|HAZARD_CONSTRUCTION_TRAFFIC|construction traffic merging|||||
HAZARD|HAZARD_CONVOY|convoy(s)|$Q convoy(s)|q_int||0.8|
HAZARD|HAZARD_CONVOY_CLEARED|convoy cleared|||||
HAZARD|HAZARD_CYCLISTS_ON_ROAD|cyclists on roadway||||0.8|
HAZARD|HAZARD_DANGEROUS_VEHICLE_CLEARED|dangerous vehicle warning cleared|||||
HAZARD|HAZARD_DEEP_SNOW|deep snow||||0.8|
HAZARD|HAZARD_DEEP_SNOW_ABOVE_ELEVATION|deep snow|deep snow above $Q hundred meters|q_int||Deprecated|Use HAZARD_DEEP_SNOW, S_PLACE_HIGH_ALTITUDE, q_dimension instead
HAZARD|HAZARD_DIFFICULT_CONDITIONS|difficult driving conditions||||0.8|
HAZARD|HAZARD_DIFFICULT_CONDITIONS_ABOVE_ELEVATION|difficult driving conditions|difficult driving conditions above $Q hundred meters|q_int||Deprecated|Use HAZARD_DIFFICULT_CONDITIONS, S_PLACE_HIGH_ALTITUDE, q_dimension instead
HAZARD|HAZARD_DRIVING_CONDITIONS_IMPROVED|driving conditions improved|||||
HAZARD|HAZARD_EARTHQUAKE_DAMAGE|earthquake damage||||0.8|
HAZARD|HAZARD_EMERGENCY_VEHICLE|emergency vehicles|$Q emergency vehicles|q_int||0.8|
HAZARD|HAZARD_EMERGENCY_VEHICLE_CLEARED|emergency vehicle warning cleared|||||
HAZARD|HAZARD_EXCEPTIONAL_LOAD_CLEARED|exceptional load warning cleared|||||
HAZARD|HAZARD_EXTREMELY_HAZARDOUS_CONDITIONS|extremely hazardous driving conditions||||0.8|
HAZARD|HAZARD_EXTREMELY_HAZARDOUS_CONDITIONS_ABOVE_ELEVATION|extremely hazardous driving conditions|extremely hazardous driving conditions above $Q hundred meters|q_int||Deprecated|Use HAZARD_EXTREMELY_HAZARDOUS_CONDITIONS, S_PLACE_HIGH_ALTITUDE, q_dimension instead
HAZARD|HAZARD_FALLEN_POWER_CABLES|fallen power cables||||0.8|
HAZARD|HAZARD_FALLEN_TREE|fallen trees|$Q fallen trees|q_int||0.8|
HAZARD|HAZARD_FLASH_FLOODS|flash floods||||0.8|
HAZARD|HAZARD_FLASH_FLOODS_LIKELY|danger of flash floods|||||
HAZARD|HAZARD_FLOODING|flooding||||0.8|
HAZARD|HAZARD_FOREST_FIRE|forest fire||||0.8|
HAZARD|HAZARD_FREEZING_RAIN|freezing rain||||0.8|
HAZARD|HAZARD_FREEZING_RAIN_ABOVE_ELEVATION|freezing rain|freezing rain above $Q hundred meters|q_int||Deprecated|Use HAZARD_FREEZING_RAIN, S_PLACE_HIGH_ALTITUDE, q_dimension instead
HAZARD|HAZARD_FRESH_SNOW|fresh snow||||0.8|
HAZARD|HAZARD_FRESH_SNOW_ABOVE_ELEVATION|fresh snow|fresh snow above $Q hundred meters|q_int||Deprecated|Use HAZARD_FRESH_SNOW, S_PLACE_HIGH_ALTITUDE, q_dimension instead
HAZARD|HAZARD_FUEL_ON_ROAD|fuel on road||||0.8|
HAZARD|HAZARD_GAS_LEAK|gas leak||||0.8|
HAZARD|HAZARD_GRASS_FIRE|grass fire||||0.8|
HAZARD|HAZARD_GRITTING_VEHICLE|salting vehicles|$Q salting vehicles|q_int||0.8|
HAZARD|HAZARD_HAZARDOUS_CONDITIONS|hazardous driving conditions||||0.8|
HAZARD|HAZARD_HAZARDOUS_CONDITIONS_ABOVE_ELEVATION|hazardous driving conditions|hazardous driving conditions above $Q hundred meters|q_int||Deprecated|Use HAZARD_HAZARDOUS_CONDITIONS, S_PLACE_HIGH_ALTITUDE, q_dimension instead
HAZARD|HAZARD_HAZARDOUS_LOAD_CLEARED|hazardous load warning cleared|||||
HAZARD|HAZARD_HAZMAT|vehicle(s) carrying hazardous materials|$Q vehicle(s) carrying hazardous materials|q_int||0.8|
HAZARD|HAZARD_HIGH_SPEED_CHASE|high-speed chase|high-speed chase involving $Q vehicles|q_int||0.8|
HAZARD|HAZARD_HIGH_SPEED_EMERGENCY_VEHICLE|high-speed emergency vehicles|$Q high-speed emergency vehicles|q_int||0.8|
HAZARD|HAZARD_HOUSE_FIRE|house fire||||0.8|
HAZARD|HAZARD_ICE|ice||||0.8|
HAZARD|HAZARD_ICE_ABOVE_ELEVATION|ice|ice above $Q hundred meters|q_int||Deprecated|Use HAZARD_ICE, S_PLACE_HIGH_ALTITUDE, q_dimension instead
HAZARD|HAZARD_ICE_BUILDUP|ice build-up||||0.8|
HAZARD|HAZARD_ICE_LIKELY|danger of ice||||0.8|
HAZARD|HAZARD_ICE_LIKELY_ABOVE_ELEVATION|danger of ice|danger of ice above $Q hundred meters|q_int||Deprecated|Use HAZARD_ICE_LIKELY, S_PLACE_HIGH_ALTITUDE, q_dimension instead
HAZARD|HAZARD_ICE_ON_CABLE|ice build-up on cable structure|||||
HAZARD|HAZARD_ICY_PATCHES|icy patches||||0.8|
HAZARD|HAZARD_ICY_PATCHES_ABOVE_ELEVATION|icy patches|icy patches above $Q hundred meters|q_int||Deprecated|Use HAZARD_ICY_PATCHES, S_PLACE_HIGH_ALTITUDE, q_dimension instead
HAZARD|HAZARD_ICY_PATCHES_LIKELY|danger of icy patches||||0.8|
HAZARD|HAZARD_ICY_PATCHES_LIKELY_ABOVE_ELEVATION|danger of icy patches|danger of icy patches above $Q hundred meters|q_int||Deprecated|Use HAZARD_ICY_PATCHES_LIKELY, S_PLACE_HIGH_ALTITUDE, q_dimension instead
HAZARD|HAZARD_IMPASSABLE|impassable||||0.8|
HAZARD|HAZARD_IMPASSABLE_ABOVE_ELEVATION|impassable|impassable above $Q hundred meters|q_int||Deprecated|Use HAZARD_IMPASSABLE, S_PLACE_HIGH_ALTITUDE, q_dimension instead
HAZARD|HAZARD_IMPASSABLE_OVER_WEIGHT|impassable for heavy vehicles|impassable for heavy vehicles over $Q|q_weight||Deprecated|Use HAZARD_IMPASSABLE and S_VEHICLE_HEAVY instead
HAZARD|HAZARD_LANDSLIPS|landslips||||0.8|
HAZARD|HAZARD_LARGE_ANIMALS_ON_ROAD|large animals on roadway||||0.8|
HAZARD|HAZARD_LEAVES_ON_ROAD|leaves on road||||0.8|
HAZARD|HAZARD_LONG_LOAD|long load(s)|$Q long load(s)|q_int||0.8|
HAZARD|HAZARD_LOOSE_CHIPPINGS|loose chippings||||0.8|
HAZARD|HAZARD_LOOSE_SAND_ON_ROAD|loose sand on road||||0.8|
HAZARD|HAZARD_MEDICAL_EMERGENCY|medical emergency ongoing|||||
HAZARD|HAZARD_MILITARY_CONVOY|military convoy(s)|$Q military convoy(s)|q_int||0.8|
HAZARD|HAZARD_MUD_ON_ROAD|mud on road||||0.8|
HAZARD|HAZARD_MUD_SLIDE|mud slide||||0.8|
HAZARD|HAZARD_OBJECT_ON_ROAD|object(s) on roadway|$Q object(s) on roadway|q_int||0.8|
HAZARD|HAZARD_OBJECTS_FALLING_FROM_MOVING_VEHICLE|objects falling from moving vehicle||||0.8|
HAZARD|HAZARD_OBSTRUCTION|obstruction(s) on roadway|$Q obstruction(s) on roadway|q_int||0.8|
HAZARD|HAZARD_OBSTRUCTION_WARNING_WITHDRAWN|obstruction warning withdrawn|||||
HAZARD|HAZARD_OIL_ON_ROAD|oil on road||||0.8|
HAZARD|HAZARD_ON_ENTRY|dangerous situation on entry ramp|||||
HAZARD|HAZARD_ON_EXIT|dangerous situation on exit ramp|||||
HAZARD|HAZARD_OVERHEIGHT_LOAD|overheight vehicle(s)|$Q overheight vehicle(s)|q_int||0.8|
HAZARD|HAZARD_PACKED_SNOW|packed snow||||0.8|
HAZARD|HAZARD_PACKED_SNOW_ABOVE_ELEVATION|packed snow|packed snow above $Q hundred meters|q_int||Deprecated|Use HAZARD_PACKED_SNOW, S_PLACE_HIGH_ALTITUDE, q_dimension instead
HAZARD|HAZARD_PASSABLE|passable||||0.8|
HAZARD|HAZARD_PASSABLE_BELOW_ELEVATION|passable|passable up to $Q hundred meters|q_int||Deprecated|Use HAZARD_PASSABLE, S_PLACE_LOW_ALTITUDE, q_dimension instead
HAZARD|HAZARD_PASSABLE_WITH_CARE|passable with care||||0.8|
HAZARD|HAZARD_PASSABLE_WITH_CARE_ABOVE_ELEVATION|passable with care|passable with care above $Q hundred meters|q_int||Deprecated|Use HAZARD_PASSABLE_WITH_CARE, S_PLACE_HIGH_ALTITUDE, q_dimension instead
HAZARD|HAZARD_PASSABLE_WITH_CARE_BELOW_ELEVATION|passable with care|passable with care up to $Q hundred meters|q_int||Deprecated|Use HAZARD_PASSABLE_WITH_CARE, S_PLACE_LOW_AREA, q_dimension instead
HAZARD|HAZARD_PEOPLE_ON_ROAD|people on roadway||||0.8|
HAZARD|HAZARD_PEOPLE_THROWING_OBJECTS|people throwing objects onto the road. Danger|||||
HAZARD|HAZARD_POLICE_ACTIVITY|police activity ongoing|||||
HAZARD|HAZARD_PROHIBITED_VEHICLE|prohibited vehicle(s) on the roadway|$Q prohibited vehicle(s) on the roadway|q_int||0.8|
HAZARD|HAZARD_RAIL_CRASH|rail crash||||0.8|
HAZARD|HAZARD_RAIN_CHANGING_TO_SNOW|rain changing to snow||||0.8|
HAZARD|HAZARD_RECKLESS_DRIVER|reckless driver(s)|$Q reckless driver(s)|q_int||0.8|
HAZARD|HAZARD_ROAD_CLEARED|road cleared|||||
HAZARD|HAZARD_ROAD_CONDITIONS_IMPROVED|conditions of road surface improved|||||
HAZARD|HAZARD_ROAD_FREE|road free again|||||
HAZARD|HAZARD_ROAD_MARKING_WORK|road marking work|$Q sets of road marking work|q_int||0.8|
HAZARD|HAZARD_ROAD_SALTED|road salted|||||
HAZARD|HAZARD_ROCKFALLS|rockfalls||||0.8|
HAZARD|HAZARD_SERIOUS_FIRE|serious fire||||0.8|
HAZARD|HAZARD_SEWER_OVERFLOW|sewer overflow||||0.8|
HAZARD|HAZARD_SHED_LOAD|shed load(s)|$Q shed load(s)|q_int||0.8|
HAZARD|HAZARD_SIGHTSEERS_OBSTRUCTING_ACCESS|sightseers obstructing access||||0.8|
HAZARD|HAZARD_SKID_HAZARD_REDUCED|skid hazard reduced|||||
HAZARD|HAZARD_SLIPPERY_ROAD|slippery road||||0.8|
HAZARD|HAZARD_SLIPPERY_ROAD_ABOVE_ELEVATION|slippery road|slippery road above $Q hundred meters|q_int||Deprecated|Use HAZARD_SLIPPERY_ROAD, S_PLACE_HIGH_ALTITUDE, q_dimension instead
HAZARD|HAZARD_SLOW_MOVING_MAINTENANCE|slow moving maintenance vehicle(s)|$Q slow moving maintenance vehicle(s)|q_int||0.8|
HAZARD|HAZARD_SLOW_VEHICLE|slow vehicle(s)|$Q slow vehicle(s)|q_int||0.8|
HAZARD|HAZARD_SLUSH|slush||||0.8|
HAZARD|HAZARD_SLUSH_ABOVE_ELEVATION|slush|slush above $Q hundred meters|q_int||Deprecated|Use HAZARD_SLUSH, S_PLACE_HIGH_ALTITUDE, q_dimension instead
HAZARD|HAZARD_SNOW_AND_ICE_DEBRIS|snow and ice debris||||0.8|
HAZARD|HAZARD_SNOW_AND_ICE_DEBRIS_LIKELY|danger of snow and ice debris|||||
HAZARD|HAZARD_SNOW_CHANGING_TO_RAIN|snow changing to rain||||0.8|
HAZARD|HAZARD_SNOW_CLEARED|snow cleared|||||
HAZARD|HAZARD_SNOW_DRIFTS|snow drifts||||0.8|
HAZARD|HAZARD_SNOW_DRIFTS_ABOVE_ELEVATION|snow drifts|snow drifts above $Q hundred meters|q_int||Deprecated|Use HAZARD_SNOW_DRIFTS, S_PLACE_HIGH_ALTITUDE, q_dimension instead
HAZARD|HAZARD_SNOW_ON_ROAD|snow on the road||||0.8|
HAZARD|HAZARD_SNOW_ON_ROAD_ABOVE_ELEVATION|snow on the road|snow on the road above $Q hundred meters|q_int||Deprecated|Use HAZARD_SNOW_ON_ROAD, S_PLACE_HIGH_ALTITUDE, q_dimension instead
HAZARD|HAZARD_SNOW_PATCHES|snow patches|||||
HAZARD|HAZARD_SNOW_PATCHES_LIKELY|danger of snow patches|||||
HAZARD|HAZARD_SNOWPLOW|snowploughs|$Q snowploughs|q_int||0.8|
HAZARD|HAZARD_SPILLAGE|spillage on the road||||0.8|
HAZARD|HAZARD_SPILLAGE_FROM_MOVING_VEHICLE|spillage occurring from moving vehicle||||0.8|
HAZARD|HAZARD_STORM_DAMAGE|storm damage||||0.8|
HAZARD|HAZARD_SUBSIDENCE|subsidence||||0.8|
HAZARD|HAZARD_SURFACE_WATER|surface water hazard||||0.8|
HAZARD|HAZARD_TRACK_LAYING_VEHICLE|track-laying vehicle(s)|$Q track-laying vehicle(s)|q_int||0.8|
HAZARD|HAZARD_TRAILER_IMPASSABLE_ABOVE_ELEVATION|impassable for vehicles with trailers|impassable above $Q hundred meters for vehicles with trailers|q_int||Deprecated|Use HAZARD_IMPASSABLE, S_VEHICLE_WITH_TRAILER, S_PLACE_HIGH_ALTITUDE, q_dimension instead
HAZARD|HAZARD_UNLIT_VEHICLE|unlit vehicle(s) on the road|$Q unlit vehicle(s) on the road|q_int||0.8|
HAZARD|HAZARD_UNLIT_VEHICLE_LIKELY|danger of unlit vehicle(s) on the road|danger of $Q unlit vehicle(s) on the road|q_int|||
HAZARD|HAZARD_UNPROTECTED_ACCIDENT|unprotected accident area(s)|$Q unprotected accident area(s)|q_int||0.8|
HAZARD|HAZARD_UNPROTECTED_ACCIDENT_LIKELY|danger of unprotected accident area(s)|danger of $Q unprotected accident area(s)|q_int|||
HAZARD|HAZARD_VEHICLE_STUCK_UNDER_BRIDGE|vehicle stuck under bridge||||0.8|
HAZARD|HAZARD_VOLCANO_ERUPTION|volcano eruption warning|||||
HAZARD|HAZARD_WAITING_VEHICLES|danger of waiting vehicles on roadway|||||
HAZARD|HAZARD_WARNING_CLEARED|warning cleared|||||
HAZARD|HAZARD_WET_ICY_ROADS|wet and icy roads||||0.8|
HAZARD|HAZARD_WET_ICY_ROADS_ABOVE_ELEVATION|wet and icy roads|wet and icy roads above $Q hundred meters|q_int||Deprecated|Use HAZARD_WET_ICY_ROADS, S_PLACE_HIGH_ALTITUDE, q_dimension instead
HAZARD|HAZARD_WIDE_LOAD|wide load(s)|$Q wide load(s)|q_int|||
HAZARD|HAZARD_WRONG_WAY_DRIVER|vehicle(s) on wrong carriageway|$Q vehicle(s) on wrong carriageway|q_int||0.8|
INCIDENT|(null)|||||0.8|
INCIDENT|INCIDENT_ACCIDENT|accident(s)|$Q accident(s)|q_int||0.8|
INCIDENT|INCIDENT_ACCIDENT_CLEARED|accident cleared|||||
INCIDENT|INCIDENT_ACCIDENT_IN_ROADWORKS_AREA|accident(s) in roadworks area|$Q accident(s) in roadworks area|q_int|||
INCIDENT|INCIDENT_ACCIDENT_OPPOSING_LANE|accident(s) in the opposing lanes|$Q accident(s) in the opposing lanes|q_int|||
INCIDENT|INCIDENT_ALL_ACCIDENTS_CLEARED|all accidents cleared, no problems to report|||||
INCIDENT|INCIDENT_BROKEN_DOWN_BUS|broken down bus(es)|$Q broken down bus(es)|q_int||0.8|
INCIDENT|INCIDENT_BROKEN_DOWN_HGV|broken down heavy truck(s)|$Q broken down heavy truck(s)|q_int||0.8|
INCIDENT|INCIDENT_BROKEN_DOWN_VEHICLE|broken down vehicle(s)|$Q broken down vehicle(s)|q_int||0.8|
INCIDENT|INCIDENT_BUS_ACCIDENT|accident involving (a) bus(es)|accident involving a/$Q bus(es)|q_int||0.8|
INCIDENT|INCIDENT_CHEMICAL_SPILLAGE|chemical spillage accident(s)|$Q chemical spillage accident(s)|q_int||0.8|
INCIDENT|INCIDENT_CLEARANCE|clearance work||||0.8|
INCIDENT|INCIDENT_EARLIER_ACCIDENT|earlier accident(s)|$Q earlier accident(s)|q_int||0.8|
INCIDENT|INCIDENT_FUEL_SPILLAGE|fuel spillage accident(s)|$Q fuel spillage accident(s)|q_int||0.8|
INCIDENT|INCIDENT_GENERIC|incident(s)|$Q incident(s)|q_int||0.8|
INCIDENT|INCIDENT_HAZMAT_ACCIDENT|accident(s) involving hazardous materials|$Q accident(s) involving hazardous materials|q_int||0.8|
INCIDENT|INCIDENT_HGV_ACCIDENT|accident involving (a) heavy lorr(y/ies)|accident involving a/$Q heavy lorr(y/ies)|q_int||0.8|
INCIDENT|INCIDENT_INCIDENT_CLEARED|incident cleared|||||
INCIDENT|INCIDENT_INVESTIGATION|accident investigation work||||0.8|
INCIDENT|INCIDENT_JACKKNIFED_CARAVAN|jackknifed caravan(s)|$Q jackknifed caravan(s)|q_int||0.8|
INCIDENT|INCIDENT_JACKKNIFED_HGV|jackknifed articulated truck(s)|$Q jackknifed articulated truck(s)|q_int||0.8|
INCIDENT|INCIDENT_JACKKNIFED_TRAILER|jackknifed trailer(s)|$Q jackknifed trailer(s)|q_int||0.8|
INCIDENT|INCIDENT_MULTI_VEHICLE_ACCIDENT|multi-vehicle accident|multi-vehicle accident involving $Q vehicles|q_int||0.8|
INCIDENT|INCIDENT_OIL_SPILLAGE|oil spillage accident(s)|$Q oil spillage accident(s)|q_int||0.8|
INCIDENT|INCIDENT_OVERHEIGHT|overheight warning system triggered|||||
INCIDENT|INCIDENT_OVERTURNED_HGV|overturned heavy truck(s)|$Q overturned heavy truck(s)|q_int||0.8|
INCIDENT|INCIDENT_OVERTURNED_VEHICLE|overturned vehicle(s)|$Q overturned vehicle(s)|q_int||0.8|
INCIDENT|INCIDENT_RESCUE|rescue and recovery work in progress||||0.8|
INCIDENT|INCIDENT_ROAD_CLEARED|road cleared|||||
INCIDENT|INCIDENT_RUBBERNECKING|vehicles slowing to look at accident(s)|vehicles slowing to look at $Q accident(s)|q_int||0.8|
INCIDENT|INCIDENT_SECONDARY_ACCIDENT|secondary accident(s)|$Q secondary accident(s)|q_int||0.8|
INCIDENT|INCIDENT_SERIOUS_ACCIDENT|serious accident(s)|$Q serious accident(s)|q_int||0.8|
INCIDENT|INCIDENT_SPUN_VEHICLE|vehicle(s) spun around|$Q vehicle(s) spun around|q_int||0.8|
INCIDENT|INCIDENT_VEHICLE_FIRE|vehicle fire(s)|$Q vehicle fire(s)|q_int||0.8|
PARKING|(null)|||||Reserved|
PARKING|PARKING_ALL_CAR_PARKS_FULL|all car parks full|all car parks $Q full|q_percent|||
PARKING|PARKING_CAR_PARK_CLOSED|car park closed|||||
PARKING|PARKING_CAR_PARK_FULL|car park full|car park $Q full|q_percent|||
PARKING|PARKING_FEW_SPACES_AVAILABLE|only a few parking spaces available|||||
PARKING|PARKING_MULTI_STORY_CAR_PARKS_FULL|multi story car parks full|||||
PARKING|PARKING_NO_INFORMATION|no parking information available|||||
PARKING|PARKING_NO_P_AND_R_INFORMATION|no park and ride information available|||||
PARKING|PARKING_NO_PARKING_UNTIL|no parking|||||
PARKING|PARKING_NORMAL_RESTRICTIONS_LIFTED|normal parking restrictions lifted|||||
PARKING|PARKING_NUMBER_OF_SPACES_CONSTANT|number of parking spaces constant|||||
PARKING|PARKING_NUMBER_OF_SPACES_DECREASING|number of parking spaces decreasing|||||
PARKING|PARKING_NUMBER_OF_SPACES_INCREASING|number of parking spaces increasing|||||
PARKING|PARKING_P_AND_R_INFORMATION_RESUMED|park and ride information service resumed|||||
PARKING|PARKING_P_AND_R_REGULAR_SERVICE|no problems to report with park and ride services|||||
PARKING|PARKING_SPACES_AVAILABLE|parking spaces available|$Q parking spaces available|q_int|||
PARKING|PARKING_SPECIAL_RESTRICTIONS|special parking restrictions in force|||||
PARKING|PARKING_SPECIAL_RESTRICTIONS_LIFTED|special parking restrictions lifted|||||
RESTRICTION|(null)|||||0.1|
RESTRICTION|RESTRICTION_ACCESS_RESTRICTIONS_LIFTED|traffic restrictions lifted||||0.1|
RESTRICTION|RESTRICTION_ALL_CARRIAGEWAYS_CLEARED|all carriageways cleared||||0.1|
RESTRICTION|RESTRICTION_ALL_CARRIAGEWAYS_REOPENED|all carriageways reopened||||0.1|
RESTRICTION|RESTRICTION_BATCH_SERVICE|batch service||||0.1|
RESTRICTION|RESTRICTION_BLOCKED|blocked||||0.1|
RESTRICTION|RESTRICTION_BLOCKED_AHEAD|blocked ahead||||0.1|
RESTRICTION|RESTRICTION_BRIDGE_BLOCKED|bridge blocked|||||
RESTRICTION|RESTRICTION_BRIDGE_CLOSED|bridge closed|||||
RESTRICTION|RESTRICTION_CARRIAGEWAY_BLOCKED|carriageway blocked||||0.1|
RESTRICTION|RESTRICTION_CARRIAGEWAY_CLOSED|carriageway closed||||0.1|
RESTRICTION|RESTRICTION_CLOSED|closed||||0.1|q_time to indicate end time is deprecated as of 0.8, use end time instead
RESTRICTION|RESTRICTION_CLOSED_AHEAD|closed ahead||||0.1|
RESTRICTION|RESTRICTION_CONTRAFLOW|contraflow||||0.4|
RESTRICTION|RESTRICTION_CONTRAFLOW_REMOVED|contraflow removed||||Deprecated|Use a cancellation event instead
RESTRICTION|RESTRICTION_CONVOY_SERVICE_REQUIRED|convoy service required||||0.8|
RESTRICTION|RESTRICTION_ENTRY_BLOCKED|entry ramp blocked|$Q th entry ramp blocked|q_int||0.1|
RESTRICTION|RESTRICTION_ENTRY_REOPENED|entry reopened||||0.1|
RESTRICTION|RESTRICTION_EXIT_BLOCKED|exit ramp blocked|$Q th exit ramp blocked|q_int||0.1|
RESTRICTION|RESTRICTION_EXIT_REOPENED|exit reopened||||0.1|
RESTRICTION|RESTRICTION_FIREMEN_DIRECTING_TRAFFIC|firemen directing traffic|||||Use RESTRICTION_TRAFFIC_DIRECTED_MANUALLY instead
RESTRICTION|RESTRICTION_HAZMAT_STOP|vehicles carrying hazardous materials have to stop at next safe place||||Reserved|
RESTRICTION|RESTRICTION_HGV_STOP_RECOMMENDED_OVER_WEIGHT|drivers of heavy trucks are recommended to stop at next safe place|drivers of heavy trucks over $Q are recommended to stop at next safe place|q_weight||Reserved|
RESTRICTION|RESTRICTION_INTERMITTENT_CLOSURES|intermittent short term closures||||0.1|
RESTRICTION|RESTRICTION_LANE_BLOCKAGE_CLEARED|lane blockages cleared||||Deprecated|Use a cancellation event instead
RESTRICTION|RESTRICTION_LANE_BLOCKED|lane(s) blocked|$Q lane(s) blocked|q_int||0.4|
RESTRICTION|RESTRICTION_LANE_CLOSED|lane(s) closed|$Q lane(s) closed|q_int||0.4|
RESTRICTION|RESTRICTION_LANE_CLOSURE_REMOVED|lane closures removed||||Deprecated|Use a cancellation event instead
RESTRICTION|RESTRICTION_LANE_REGULATIONS_RESTORED|normal lane regulations restored||||Deprecated|Use a cancellation event instead
RESTRICTION|RESTRICTION_LANE_RESTRICTIONS_LIFTED|lane restrictions lifted||||Deprecated|Use a cancellation event instead
RESTRICTION|RESTRICTION_LET_EMERGENCY_SERVICES_PASS|allow emergency vehicles to pass||||0.8|
RESTRICTION|RESTRICTION_LOW_EMISSION_ZONE|Low Emission Zone restriction in force||||Reserved|
RESTRICTION|RESTRICTION_MAX_AXLE_LOAD|axle load limit|axle load limit $Q|q_weight||0.8|
RESTRICTION|RESTRICTION_MAX_HEIGHT|height limit|height limit $Q|q_dimension||0.8|
RESTRICTION|RESTRICTION_MAX_LENGTH|length limit|length limit $Q|q_dimension||0.8|
RESTRICTION|RESTRICTION_MAX_WEIGHT|closed for heavy vehicles|closed for heavy vehicles over $Q|q_weight||0.8|
RESTRICTION|RESTRICTION_MAX_WIDTH|width limit|width limit $Q|q_dimension||0.8|
RESTRICTION|RESTRICTION_N_ENTRIES_CLOSED|entry ramp(s) closed|$Q entry ramp(s) closed|q_int||Reserved|
RESTRICTION|RESTRICTION_N_EXITS_CLOSED|exit ramp(s) closed|$Q exit ramp(s) closed|q_int||Reserved|
RESTRICTION|RESTRICTION_NARROW_LANES|narrow lanes||||0.8|
RESTRICTION|RESTRICTION_NO_OVERTAKING|overtaking prohibited||||0.8|
RESTRICTION|RESTRICTION_NO_OVERTAKING_OVER_WEIGHT|overtaking prohibited for heavy vehicles|overtaking prohibited for heavy vehicles over $Q|q_weight||Deprecated|Use RESTRICTION_NO_OVERTAKING and S_VEHICLE_HEAVY instead
RESTRICTION|RESTRICTION_NTH_ENTRY_CLOSED|entry ramp closed|$Q th entry ramp closed|q_int||Reserved|
RESTRICTION|RESTRICTION_NTH_EXIT_CLOSED|exit ramp closed|$Q th exit ramp closed|q_int||Reserved|
RESTRICTION|RESTRICTION_OPEN|open||||0.1|
RESTRICTION|RESTRICTION_OVERTAKING_RESTRICTION_LIFTED|overtaking restriction lifted||||Deprecated|Use a cancellation event instead
RESTRICTION|RESTRICTION_POLICE_DIRECTING_TRAFFIC|police directing traffic|||||Use RESTRICTION_TRAFFIC_DIRECTED_MANUALLY instead
RESTRICTION|RESTRICTION_RAMP_BLOCKED|ramps blocked||||0.1|
RESTRICTION|RESTRICTION_RAMP_CLOSED|ramps closed||||0.1|
RESTRICTION|RESTRICTION_RAMP_REOPENED|ramps reopened||||0.1|
RESTRICTION|RESTRICTION_RAMP_RESTRICTIONS|ramp restrictions||||Reserved|
RESTRICTION|RESTRICTION_REDUCED_LANES|reduced number of lanes|carriageway reduced from $Q lanes to $Q lanes|q_ints||0.4|
RESTRICTION|RESTRICTION_REOPENED|reopened||||0.1|
RESTRICTION|RESTRICTION_RESTRICTIONS|restrictions||||0.8|
RESTRICTION|RESTRICTION_ROAD_CLEARED|road cleared||||0.1|
RESTRICTION|RESTRICTION_SERVICE_AREA_BUSY|service area busy||||Reserved|
RESTRICTION|RESTRICTION_SERVICE_AREA_FUEL_STATION_CLOSED|service area, fuel station closed||||Reserved|
RESTRICTION|RESTRICTION_SERVICE_AREA_FUEL_STATION_REOPENED|fuel station reopened||||Reserved|
RESTRICTION|RESTRICTION_SERVICE_AREA_OVERCROWDED|service area overcrowded, drive to another service area||||Reserved|
RESTRICTION|RESTRICTION_SERVICE_AREA_RESTAURANT_CLOSED|service area, restaurant closed||||Reserved|
RESTRICTION|RESTRICTION_SERVICE_AREA_RESTAURANT_REOPENED|restaurant reopened||||Reserved|
RESTRICTION|RESTRICTION_SHOULDER_BLOCKED|paved shoulder blocked||||0.8|
RESTRICTION|RESTRICTION_SINGLE_ALTERNATE_LINE_TRAFFIC|single alternate line traffic||||0.1|
RESTRICTION|RESTRICTION_SMOG_ALERT|smog alert||||Deprecated|Use ENVIRONMENT_SMOG instead; if restrictions are in place, add these separately
RESTRICTION|RESTRICTION_SMOG_ALERT_ENDED|smog alert ended||||Deprecated|Use a cancellation event instead
RESTRICTION|RESTRICTION_SPEED_LIMIT|mandatory speed limit in force|mandatory speed limit $Q in force||max|0.1|
RESTRICTION|RESTRICTION_SPEED_LIMIT_LIFTED|mandatory speed limit lifted||||0.1|
RESTRICTION|RESTRICTION_TEMP_MAX_AXLE_LOAD|temporary axle load limit|temporary axle load limit $Q|q_weight||Deprecated|Use RESTRICTION_MAX_AXLE_LOAD instead
RESTRICTION|RESTRICTION_TEMP_MAX_AXLE_LOAD_LIFTED|temporary axle weight limit lifted||||Deprecated|Use a cancellation event instead
RESTRICTION|RESTRICTION_TEMP_MAX_HEIGHT|temporary height limit|temporary height limit $Q|q_dimension||Deprecated|Use RESTRICTION_MAX_HEIGHT instead
RESTRICTION|RESTRICTION_TEMP_MAX_HEIGHT_LIFTED|temporary height limit lifted||||Deprecated|Use a cancellation event instead
RESTRICTION|RESTRICTION_TEMP_MAX_LENGTH|temporary length limit|temporary length limit $Q|q_dimension||Deprecated|Use RESTRICTION_MAX_LENGTH instead
RESTRICTION|RESTRICTION_TEMP_MAX_LENGTH_LIFTED|temporary length limit lifted||||Deprecated|Use a cancellation event instead
RESTRICTION|RESTRICTION_TEMP_MAX_WEIGHT|temporary gross weight limit|temporary gross weight limit $Q|q_weight||Deprecated|Use RESTRICTION_MAX_WEIGHT instead
RESTRICTION|RESTRICTION_TEMP_MAX_WEIGHT_LIFTED|temporary gross weight limit lifted||||Deprecated|Use a cancellation event instead
RESTRICTION|RESTRICTION_TEMP_MAX_WIDTH|temporary width limit|temporary width limit $Q|q_dimension||Deprecated|Use RESTRICTION_MAX_WIDTH instead
RESTRICTION|RESTRICTION_TEMP_MAX_WIDTH_LIFTED|temporary width limit lifted||||Deprecated|Use a cancellation event instead
RESTRICTION|RESTRICTION_THROUGH_HGV_MAX_WEIGHT|no through traffic for heavy trucks|no through traffic for heavy trucks over $Q|q_weight||Reserved|
RESTRICTION|RESTRICTION_TRAFFIC_DIRECTED_MANUALLY|traffic being directed manually||||0.8|
RESTRICTION|RESTRICTION_TRAFFIC_REGULATIONS_CHANGED|traffic regulations have been changed||||Reserved|
RESTRICTION|RESTRICTION_TRAFFIC_WARDENS_DIRECTING_TRAFFIC|traffic wardens directing traffic|||||Use RESTRICTION_TRAFFIC_DIRECTED_MANUALLY instead
RESTRICTION|RESTRICTION_TUNNEL_BLOCKED|tunnel blocked|||||
RESTRICTION|RESTRICTION_TUNNEL_CLOSED|tunnel closed|||||
RESTRICTION|RESTRICTION_USE_BUS_LANE|bus lane available for all vehicles||||Reserved|
RESTRICTION|RESTRICTION_USE_CARPOOL_LANE|carpool lane available for all vehicles||||Reserved|
RESTRICTION|RESTRICTION_USE_HEAVY_VEHICLE_LANE|heavy vehicle lane available for all vehicles||||Reserved|
RESTRICTION|RESTRICTION_USE_SHOULDER|use of paved shoulder allowed||||Deprecated|Use S_LANE_USAGE_USE_SHOULDER instead
SECURITY|(null)|||||0.8|
SECURITY|SECURITY_AIR_RAID|air raid, danger||||0.8|
SECURITY|SECURITY_AIR_RAID_WARNING_CANCELED|air raid warning cancelled|||||
SECURITY|SECURITY_ALERT|security alert||||0.8|
SECURITY|SECURITY_ALERT_WITHDRAWN|security alert withdrawn|||||
SECURITY|SECURITY_BOMB_ALERT|bomb alert||||0.8|
SECURITY|SECURITY_CHILD_ABDUCTION|child abduction in progress|||||
SECURITY|SECURITY_CIVIL_EMERGENCY|civil emergency||||0.8|
SECURITY|SECURITY_CIVIL_EMERGENCY_CANCELED|civil emergency cancelled|||||
SECURITY|SECURITY_EVACUATION|evacuation||||0.8|
SECURITY|SECURITY_GUNFIRE|gunfire on roadway, danger||||0.8|
SECURITY|SECURITY_INCIDENT|security incident||||0.8|
SECURITY|SECURITY_TERRORIST_INCIDENT|terrorist incident||||0.8|
SERVICE|(null)|||||Reserved|
SERVICE|SERVICE_INACTIVE|this TMC service is not active|||||
SERVICE|SERVICE_NO_INFORMATION|no information available|||||
SERVICE|SERVICE_NO_NEW_INFORMATION|no new traffic information available|||||
SERVICE|SERVICE_NO_PUBLIC_TRANSPORT_INFORMATION|no public transport information available|||||
SERVICE|SERVICE_NO_RAIL_INFORMATION|rail information service not available|||||
SERVICE|SERVICE_NO_RAPID_TRANSIT_INFORMATION|rapid transit information service not available|||||
SERVICE|SERVICE_RAIL_INFORMATION_RESUMED|rail information service resumed|||||
SERVICE|SERVICE_RAPID_TRANSIT_INFORMATION_RESUMED|rapid transit information service resumed|||||
SERVICE|SERVICE_RESUME_ANNOUNCE|active TMC service will resume|active TMC service will resume at $Q|q_time||Deprecated|Use SERVICE_INACTIVE with start/end time instead
SERVICE|SERVICE_SUSPENSION_ANNOUNCE|this TMC service is being suspended|this TMC service is being suspended at $Q|q_time||Deprecated|Use SERVICE_INACTIVE with start/end time instead
SERVICE|SERVICE_TELEPHONE_SERVICE|travel information telephone service availiable|||||
SPECIAL|(null)|||||Reserved|
SPECIAL|SPECIAL_NO_REPORT|nothing to report||||Reserved|
SPECIAL|SPECIAL_NULL|(null event)||||Reserved|
SPECIAL|SPECIAL_TEST_MESSAGE|this message is for test purposes only, please ignore|this message is for test purposes only number $Q, please ignore|q_int||Reserved|
TRANSPORT|(null)|||||0.8|
TRANSPORT|TRANSPORT_ALL_SERVICES_BOOKED_OUT|all services fully booked|||||
TRANSPORT|TRANSPORT_BRIDGE_REOPENING_EXPECTED|reopening of bridge expected|reopening of bridge expected $Q|q_time||Deprecated|Use TRANSPORT_MOVEABLE_BRIDGE_OPERATION with start/end time instead
TRANSPORT|TRANSPORT_CANCELLATIONS|cancellations||||0.8|
TRANSPORT|TRANSPORT_DELAYED|delayed until further notice||||0.8|
TRANSPORT|TRANSPORT_FREE_SHUTTLE_SERVICE|free shuttle service operating||||0.8|
TRANSPORT|TRANSPORT_IRREGULAR_BUS_SERVICE|bus services irregular|||||
TRANSPORT|TRANSPORT_IRREGULAR_RAIL_SERVICE|rail services irregular|||||
TRANSPORT|TRANSPORT_IRREGULAR_UNDERGROUND_SERVICE|underground services irregular|||||
TRANSPORT|TRANSPORT_MOVEABLE_BRIDGE_OPERATION|moveable bridge in operation||||0.8|
TRANSPORT|TRANSPORT_NO_BUS_SERVICE|bus services not operating|||||
TRANSPORT|TRANSPORT_NO_FERRY_SERVICE|ferry service not operating|||||
TRANSPORT|TRANSPORT_NO_NORMAL_SERVICE|normal services not operating|||||
TRANSPORT|TRANSPORT_NO_PUBLIC_TRANSPORT_SERVICE|public transport services not operating|||||
TRANSPORT|TRANSPORT_NO_RAIL_SERVICE|rail services not operating|||||
TRANSPORT|TRANSPORT_NO_RAIPID_TRANSIT_SERVICE|rapid transit service not operating|||||
TRANSPORT|TRANSPORT_NO_UNDERGROUND_SERVICE|underground service not operating|||||
TRANSPORT|TRANSPORT_NORMAL_SERVICE_EXPECTED|normal services expected|||||
TRANSPORT|TRANSPORT_P_AND_R_NOT_OPERATING_UNTIL|park and ride service not operating|||||
TRANSPORT|TRANSPORT_P_AND_R_OPERATING_UNTIL|park and ride service operating|||||
TRANSPORT|TRANSPORT_PUBLIC_TRANSPORT_RESUMED|normal public transport services resumed|||||
TRANSPORT|TRANSPORT_PUBLIC_TRANSPORT_STRIKE|public transport strike|||||
TRANSPORT|TRANSPORT_RESUMED|normal services resumed|||||
TRANSPORT|TRANSPORT_SERVICE_BOOKED_OUT|service(s) fully booked|$Q service(s) fully booked|q_time||0.8|
TRANSPORT|TRANSPORT_SERVICE_SUSPENDED|service suspended||||0.8|
TRANSPORT|TRANSPORT_SERVICE_WITHDRAWN|service withdrawn|$Q service withdrawn|q_time||0.8|
TRANSPORT|TRANSPORT_SHUTTLE_SERVICE|shuttle service operating||||0.8|
TRANSPORT|TRANSPORT_SPECIAL_SERVICE|special public transport services operating|||||
TRANSPORT|TRANSPORT_SUBSTITUTE_SERVICE|service not operating, substitute service available||||0.8|
TRAVEL_TIME|(null)|||||Reserved|
TRAVEL_TIME|TRAVEL_TIME_NEXT_ARRIVAL|next arrival|next arrival $Q|q_time|||
TRAVEL_TIME|TRAVEL_TIME_NEXT_DEPARTURE|next departure|next departure $Q|q_time|||
TRAVEL_TIME|TRAVEL_TIME_P_AND_R|park and ride trip time|park and ride trip time $Q|q_duration|||
TRAVEL_TIME|TRAVEL_TIME_TRIP|current trip time|current trip time $Q|q_duration|||
WEATHER|(null)|||||0.8|
WEATHER|WEATHER_BLIZZARD|blizzard|blizzard, visibility reduced to $Q|q_dimension||0.8|
WEATHER|WEATHER_BLOWING_DUST|blowing dust|blowing dust, visibility reduced to $Q|q_dimension||0.8|
WEATHER|WEATHER_BLOWING_SNOW|blowing snow|blowing snow, visibility reduced to $Q|q_dimension||0.8|
WEATHER|WEATHER_CALM|calm weather|||||
WEATHER|WEATHER_CLEAR|clear weather||q_percent|||
WEATHER|WEATHER_CROSSWIND|crosswinds|crosswinds $Q|q_speed||0.8|
WEATHER|WEATHER_DAMAGING_HAIL|damaging hail|damaging hail, visibility reduced to $Q|q_dimension||0.8|
WEATHER|WEATHER_DENSE_FOG|dense fog|dense fog, visibility reduced to $Q|q_dimension||0.8|
WEATHER|WEATHER_DRY|dry weather||q_percent|||
WEATHER|WEATHER_EXTREME_COLD|extreme cold|extreme cold of $Q|q_temperature||0.8|
WEATHER|WEATHER_EXTREME_HEAT|extreme heat|extreme heat up to $Q|q_temperature||0.8|
WEATHER|WEATHER_FOG|fog|fog, visibility reduced to $Q|q_dimension||0.8|
WEATHER|WEATHER_FOG_CLEARING|fog clearing|||||
WEATHER|WEATHER_FOG_FORECAST_WITHDRAWN|fog forecast withdrawn|||||
WEATHER|WEATHER_FORECAST_COLD_MAX|cold|cold, maximum temperature $Q|q_temperature|||
WEATHER|WEATHER_FORECAST_COLD_MIN|cold|cold, minimum temperature $Q|q_temperature|||
WEATHER|WEATHER_FORECAST_COOL_MAX|cool|cool, maximum temperature $Q|q_temperature|||
WEATHER|WEATHER_FORECAST_COOL_MIN|cool|cool, minimum temperature $Q|q_temperature|||
WEATHER|WEATHER_FORECAST_HOT_MAX|hot|hot ,maximum temperature $Q|q_temperature|||
WEATHER|WEATHER_FORECAST_MILD_MAX|mild|mild, maximum temperature $Q|q_temperature|||
WEATHER|WEATHER_FORECAST_MILD_MIN|mild|mild, minimum temperature $Q|q_temperature|||
WEATHER|WEATHER_FORECAST_VERY_COLD_MAX|very cold|very cold, maximum temperature $Q|q_temperature|||
WEATHER|WEATHER_FORECAST_VERY_COLD_MIN|very cold|very cold, minimum temperature $Q|q_temperature|||
WEATHER|WEATHER_FORECAST_VERY_WARM_MIN|very warm|very warm, minimum temperature $Q|q_temperature|||
WEATHER|WEATHER_FORECAST_WARM_MAX|warm|warm, maximum temperature $Q|q_temperature|||
WEATHER|WEATHER_FORECAST_WARM_MIN|warm|warm, minimum temperature $Q|q_temperature|||
WEATHER|WEATHER_FORECAST_WITHDRAWN|weather forecast withdrawn|||||
WEATHER|WEATHER_FREEZING_FOG|freezing fog|freezing fog, visibility reduced to $Q|q_dimension||0.8|
WEATHER|WEATHER_FROST|frost||||0.8|
WEATHER|WEATHER_GALES|gales|gales $Q|q_speed||0.8|
WEATHER|WEATHER_GUSTY_WIND|gusty winds|gusty winds $Q|q_speed||0.8|
WEATHER|WEATHER_HAIL|hail|hail visibility reduced to $Q|q_dimension||0.8|
WEATHER|WEATHER_HEAT|heat|||||
WEATHER|WEATHER_HEAVY_FROST|heavy frost||||0.8|
WEATHER|WEATHER_HEAVY_RAIN|heavy rain|heavy rain $Q|q_dimension||0.8|
WEATHER|WEATHER_HEAVY_SNOWFALL|heavy snowfall|heavy snowfall $Q|q_dimension||0.8|
WEATHER|WEATHER_HURRICANE|hurricane force winds|hurricane force winds $Q|q_speed||0.8|
WEATHER|WEATHER_IMPROVEMENT|weather situation improved|||||
WEATHER|WEATHER_INSECT_SWARMS|swarms of insects|swarms of insects, visibility reduced to $Q|q_dimension||0.8|
WEATHER|WEATHER_LESS_EXTREME_TEMPERATURES|less extreme temperatures|||||
WEATHER|WEATHER_LIGHT_WIND|light winds|light winds $Q|q_speed|||
WEATHER|WEATHER_LOW_SUN_GLARE|low sun glare||||0.8|
WEATHER|WEATHER_MAXIMUM_TEMPERATURE_FORECAST|maximum temperature|maximum temperature of $Q|q_temperature|||
WEATHER|WEATHER_MINIMUM_TEMPERATURE_FORECAST|minimum temperature|minimum temperature of $Q|q_temperature|||
WEATHER|WEATHER_MODERATE_WIND|moderate winds|moderate winds $Q|q_speed|||
WEATHER|WEATHER_MOSTLY_CLOUDY|mostly cloudy||q_percent|||
WEATHER|WEATHER_MOSTLY_DRY|mostly dry weather||q_percent|||
WEATHER|WEATHER_OVERCAST|overcast weather||q_percent|||
WEATHER|WEATHER_PARTLY_CLOUDY|partly cloudy||q_percent|||
WEATHER|WEATHER_PATCHY_FOG|patchy fog|patchy fog, visibility reduced to $Q|q_dimension||0.8|
WEATHER|WEATHER_RAIN|rain|rain $Q|q_dimension||0.8|
WEATHER|WEATHER_RAPID_TEMPERATURE_DROP|temperature falling rapidly|temperature falling rapidly to $Q|q_temperature||0.8|
WEATHER|WEATHER_REDUCED_VISIBILITY|visibility reduced|visibility reduced to $Q|q_dimension||0.8|
WEATHER|WEATHER_SANDSTORM|sandstorms|sandstorms, visibility reduced to $Q|q_dimension||0.8|
WEATHER|WEATHER_SHOWERS|showers|showers, visibility reduced to $Q|q_dimension||0.8|
WEATHER|WEATHER_SLEET|sleet|sleet, visibility reduced to $Q|q_dimension||0.8|
WEATHER|WEATHER_SMOKE_HAZARD|smoke hazard|smoke hazard, visibility reduced to $Q|q_dimension||0.8|
WEATHER|WEATHER_SNOWFALL|snowfall|snowfall $Q|q_dimension||0.8|
WEATHER|WEATHER_SNOWFALL_AND_FOG|snowfall and fog|snowfall and fog, visibility reduced to $Q|q_dimension|||
WEATHER|WEATHER_SPRAY_HAZARD|spray hazard|spray hazard, visibility reduced to $Q|q_dimension||0.8|
WEATHER|WEATHER_STORM|storm force winds|storm force winds $Q|q_speed||0.8|
WEATHER|WEATHER_STRONG_WIND|strong winds|strong winds $Q|q_speed||0.8|
WEATHER|WEATHER_STRONG_WIND_EASING|strong winds easing|||||
WEATHER|WEATHER_STRONG_WIND_FORECAST_WITHDRAWN|strong wind forecast withdrawn|||||
WEATHER|WEATHER_SUNNY|sunny weather||q_percent|||
WEATHER|WEATHER_SUNNY_PERIODS|sunny periods||q_percent|||
WEATHER|WEATHER_SUNNY_PERIODS_WITH_SHOWERS|sunny periods with showers||q_percent|||
WEATHER|WEATHER_TEMPERATURE|current temperature|current temperature $Q|q_temperature|||
WEATHER|WEATHER_TEMPERATURE_RISING|temperature rising|temperature rising to $Q|q_temperature|||
WEATHER|WEATHER_THUNDERSTORM|thunderstorms|thunderstorms, visibility reduced to $Q|q_dimension||0.8|
WEATHER|WEATHER_TORNADO|tornadoes||||0.8|
WEATHER|WEATHER_TORNADO_WARNING_ENDED|tornado warning ended|||||
WEATHER|WEATHER_TORNADO_WATCH_CANCELED|tornado watch cancelled|||||
WEATHER|WEATHER_VISIBILITY_IMPROVEMENT|visibility improved|||||
WEATHER|WEATHER_WARNING_WITHDRAWN|adverse weather warning withdrawn|||||
WEATHER|WEATHER_WHITEOUT|white out|white out, visibility reduced to $Q|q_dimension||0.8|
WEATHER|WEATHER_WIND_CHILL_TEMPERATURE|effective temperature, with wind chill|effective temperature, with wind chill $Q|q_temperature|||
WEATHER|WEATHER_WIND_FORECAST_WITHDRAWN|wind forecast withdrawn|||||
WEATHER|WEATHER_WINTER_STORM|winter storm|winter storm, visibility reduced to $Q|q_dimension||0.8|
