This is the repository for the Traffic Feed Format (TraFF) specification.

# Branches

This repository has two branches:

* `master` always holds the latest released version of the specification.
* `dev` holds the version currently under development. Once it is ready for publication, it will get merged into `master`.

This convention is currently being introduced across all TraFF projects.

# Working with Lists

The **TraFF Event List** and **TraFF SI List** are in a hybrid format which is at the same time valid Markdown syntax and a valid CSV file. When working with the document, please ensure you are not breaking this by keeping to the following rules:

* Do not insert text before or after the table.
* Keep the first two rows intact. The first row contains column labels, the second row (`-|-|-|-|-`) is necessary for the Markdown parser to recognize the data as a table. Should you need to insert extra columns, be sure to put a hyphen in the second row.
* Do not add any padding before or after the pipe characters; they will break things when the document is read as CSV.
* Do not use any Markdown formatting within table cells, for the same reason as above.
* When editing the document in your favorite spreadsheet application, ensure it has been saved with the pipe character (`|`) as the column separator.
* Avoid changing the sort order (or restore the previous one before you commit), in order to keep diffs legible.
