Class|Type|Text|Text_Q|Quantifier|Since|
-|-|-|-|-|-|
COURTESY|(null)||||Reserved|
COURTESY|S_COURTESY_SORRY_FOR_DELAY|sorry for any delay||||
COURTESY|S_COURTESY_THANKS_FOR_COOPERATION|we are grateful for your cooperation||||
DIRECTION|(null)||||Reserved|
DIRECTION|S_DIRECTION_E|eastbound||||
DIRECTION|S_DIRECTION_FROM|from||||
DIRECTION|S_DIRECTION_FROM_TOWN|from town||||
DIRECTION|S_DIRECTION_IN|in||||
DIRECTION|S_DIRECTION_N|northbound||||
DIRECTION|S_DIRECTION_NE|northeastbound||||
DIRECTION|S_DIRECTION_NW|northwestbound||||
DIRECTION|S_DIRECTION_ROADS_FROM|for roads from||||
DIRECTION|S_DIRECTION_ROADS_IN|for roads in||||
DIRECTION|S_DIRECTION_ROADS_TO|for roads leading towards||||
DIRECTION|S_DIRECTION_S|southbound||||
DIRECTION|S_DIRECTION_SE|southeastbound||||
DIRECTION|S_DIRECTION_SW|southwestbound||||
DIRECTION|S_DIRECTION_TO|to||||
DIRECTION|S_DIRECTION_TOWARDS_TOWN|towards town||||
DIRECTION|S_DIRECTION_W|westbound||||
DIVERSION|(null)||||0.8|
DIVERSION|S_DIVERSION_AVOID_AREA|local drivers are recommended to avoid the area|||0.8|
DIVERSION|S_DIVERSION_COMPULSORY|compulsory diversion in operation||||
DIVERSION|S_DIVERSION_FOLLOW_DIVERSION_SIGNS|follow diversion signs|||0.8|
DIVERSION|S_DIVERSION_FOLLOW_MARKERS|follow special diversion markers|||0.8|
DIVERSION|S_DIVERSION_FOLLOW_SIGNS|follow signs||||
DIVERSION|S_DIVERSION_HGV_AVOID_AREA|heavy trucks are recommended to avoid the area||||
DIVERSION|S_DIVERSION_IGNORE_SIGNS|do not follow diversion signs|||0.8|
DIVERSION|S_DIVERSION_IN_OPERATION|diversion in operation||||
DIVERSION|S_DIVERSION_LOCAL|follow local diversion|||0.8|
DIVERSION|S_DIVERSION_NO_LONGER_RECOMMENDED|diversion is no longer recommended||||
DIVERSION|S_DIVERSION_UNAVAILABLE|no suitable diversion available||||
INSTRUCTION|(null)||||0.8|
INSTRUCTION|S_INSTRUCTION_APPROACH_WITH_CARE|approach with care|||0.8|
INSTRUCTION|S_INSTRUCTION_AVOID_GAPS|do not allow unnecessary gaps|||0.8|
INSTRUCTION|S_INSTRUCTION_AVOID_TRAVELING|only travel if absolutely necessary|||0.8|
INSTRUCTION|S_INSTRUCTION_AWAIT_ESCORT|wait for escort vehicle|||0.8|
INSTRUCTION|S_INSTRUCTION_AWAIT_PATROL|in emergency, wait for police patrol|||0.8|
INSTRUCTION|S_INSTRUCTION_CAREFUL|drive carefully|||0.8|
INSTRUCTION|S_INSTRUCTION_CLEAR_EMERGENCY_LANE|clear a lane for emergency vehicles||||
INSTRUCTION|S_INSTRUCTION_CLEAR_WINTER_SERVICE_LANE|clear a lane for snowploughs and gritting vehicles||||
INSTRUCTION|S_INSTRUCTION_CROSS_WITH_CARE|cross junction with care|||0.8|
INSTRUCTION|S_INSTRUCTION_EXTREME_CAUTION|drive with extreme caution|||0.8|
INSTRUCTION|S_INSTRUCTION_FOLLOW_PRECEDING_VEHICLE|follow the vehicle in front, smoothly|||0.8|
INSTRUCTION|S_INSTRUCTION_INCREASE_DISTANCE|increase normal following distance|||0.8|
INSTRUCTION|S_INSTRUCTION_KEEP_DISTANCE|keep your distance|||0.8|
INSTRUCTION|S_INSTRUCTION_LEAVE_VEHICLE|leave your vehicle. Proceed to next safe place|||0.8|
INSTRUCTION|S_INSTRUCTION_NO_BURNING_OBJECTS|do not throw out any burning objects|||0.8|
INSTRUCTION|S_INSTRUCTION_NO_NAKED_FLAMES|no naked flames|||0.8|
INSTRUCTION|S_INSTRUCTION_NO_SLOWDOWN|do not slow down unnecessarily|||0.8|
INSTRUCTION|S_INSTRUCTION_NO_SMOKING|no smoking|||0.8|
INSTRUCTION|S_INSTRUCTION_OBSERVE_SIGNALS|observe signals|||0.8|
INSTRUCTION|S_INSTRUCTION_OBSERVE_SIGNS|observe signs|||0.8|
INSTRUCTION|S_INSTRUCTION_OVERTAKE_WITH_CARE|overtake with care|||0.8|
INSTRUCTION|S_INSTRUCTION_PULL_OVER|pull over to the edge of the roadway|||0.8|
INSTRUCTION|S_INSTRUCTION_SHUT_OFF_VENTS|close all windows. Turn off heater and vents|||0.8|
INSTRUCTION|S_INSTRUCTION_STAY_IN_VEHICLE|do not leave your vehicle|||0.8|
INSTRUCTION|S_INSTRUCTION_STOP_AT_SAFE_PLACE|stop at next safe place|||0.8|
INSTRUCTION|S_INSTRUCTION_STOP_AT_SERVICE_AREA|stop at next service area|||0.8|
INSTRUCTION|S_INSTRUCTION_SWITCH_OFF_ENGINE|switch off engine|||0.8|
INSTRUCTION|S_INSTRUCTION_SWITCH_OFF_MOBILE_PHONES|switch off mobile phones and two-way radios|||0.8|
INSTRUCTION|S_INSTRUCTION_TEST_BRAKES|test your brakes|||0.8|
INSTRUCTION|S_INSTRUCTION_USE_BUS|please use bus service|||0.8|
INSTRUCTION|S_INSTRUCTION_USE_FOG_LIGHTS|use fog lights|||0.8|
INSTRUCTION|S_INSTRUCTION_USE_HAZARD_LIGHTS|use hazard warning lights|||0.8|
INSTRUCTION|S_INSTRUCTION_USE_HEADLIGHTS|use headlights|||0.8|
INSTRUCTION|S_INSTRUCTION_USE_RAIL|please use rail service|||0.8|
INSTRUCTION|S_INSTRUCTION_USE_TRAM|please use tram service|||0.8|
INSTRUCTION|S_INSTRUCTION_USE_UNDERGROUND|please use underground service|||0.8|
LANE_USAGE|(null)||||0.8|
LANE_USAGE|S_LANE_USAGE_HEAVY_VEHICLES_USE_LEFT_LANE|heavy vehicles use left lane||||
LANE_USAGE|S_LANE_USAGE_HEAVY_VEHICLES_USE_RIGHT_LANE|heavy vehicles use right lane||||
LANE_USAGE|S_LANE_USAGE_KEEP_LEFT|keep to the left|||0.8|
LANE_USAGE|S_LANE_USAGE_KEEP_RIGHT|keep to the right|||0.8|
LANE_USAGE|S_LANE_USAGE_NO_USE_SHOULDER|do not use shoulder||||
LANE_USAGE|S_LANE_USAGE_USE_LEFT_LANE|use left lane||||
LANE_USAGE|S_LANE_USAGE_USE_LEFT_PARALLEL_CARRIAGEWAY|use left-hand parallel carriageway||||
LANE_USAGE|S_LANE_USAGE_USE_LOCAL_LANE|use local traffic lanes||||
LANE_USAGE|S_LANE_USAGE_USE_RIGHT_LANE|use right lane||||
LANE_USAGE|S_LANE_USAGE_USE_RIGHT_PARALLEL_CARRIAGEWAY|use right-hand parallel carriageway||||
LANE_USAGE|S_LANE_USAGE_USE_SHOULDER|use shoulder|||0.8|
LANE_USAGE|S_LANE_USAGE_USE_THROUGH_LANE|use through traffic lanes||||
PLACE|(null)||||0.1|
PLACE|S_PLACE_BEND|around a bend in the road|||0.8|
PLACE|S_PLACE_BRIDGE|on bridges|||0.1|
PLACE|S_PLACE_CITY_CENTER|in the city center|||0.8|
PLACE|S_PLACE_HIGH_ALTITUDE|at high altitudes|at altitudes above $Q|q_dimension|0.8|
PLACE|S_PLACE_HILL|over the crest of a hill|||0.8|
PLACE|S_PLACE_INNER_CITY|in the inner city area|||0.8|
PLACE|S_PLACE_LOW_AREA|in low-lying areas|at altitudes below $Q|q_dimension|0.8|
PLACE|S_PLACE_RAMP|on ramps|||0.1|
PLACE|S_PLACE_ROADWORKS|in the roadworks area|||0.1|
PLACE|S_PLACE_SHADE|in shaded areas|||Reserved|
PLACE|S_PLACE_TUNNEL|in tunnels|||0.1|
PLACE|S_PLACE_TUNNEL_PORTAL|at tunnel portals|||0.8|
POSITION|(null)||||Reserved|
POSITION|S_POSITION_BUS_LANE|in the bus lane||||
POSITION|S_POSITION_CARPOOL_LANE|in the carpool lane||||
POSITION|S_POSITION_CENTER|in the center||||
POSITION|S_POSITION_CONNECTING_CARRIAGEWAY|in the connecting carriageway||||
POSITION|S_POSITION_CRAWLER_LANE|in the crawler lane||||
POSITION|S_POSITION_EMERGENCY_LANE|in the emergency lane||||
POSITION|S_POSITION_EXPRESS_LANE|in the express lane||||
POSITION|S_POSITION_HEAVY_VEHICLE_LANE|in the heavy vehicle lane||||
POSITION|S_POSITION_LEFT_LANE|in the left lane||||
POSITION|S_POSITION_LEFT_PARALLEL_CARRIAGEWAY|in the left-hand parallel carriageway||||
POSITION|S_POSITION_LEFT_SIDE|on the left||||
POSITION|S_POSITION_LOCAL_LANE|in the local lane||||
POSITION|S_POSITION_MANUAL_PAYMENT_LANES|on manual payment lanes||||
POSITION|S_POSITION_MEDIAN|in the median||||
POSITION|S_POSITION_MIDDLE_LANE|in the middle lane||||
POSITION|S_POSITION_OPPOSITE_CARRIAGEWAY|on the opposite carriageway||||
POSITION|S_POSITION_OPPOSITE_DIRECTION|in the opposite direction||||
POSITION|S_POSITION_OPPOSITE_LANE|in the opposing lanes||||
POSITION|S_POSITION_OVERTAKING_LANE|in the overtaking lane||||
POSITION|S_POSITION_PARALLEL_CARRIAGEWAY|in the parallel carriageway||||
POSITION|S_POSITION_RIGHT_LANE|in the right lane||||
POSITION|S_POSITION_RIGHT_PARALLEL_CARRIAGEWAY|in the right-hand parallel carriageway||||
POSITION|S_POSITION_RIGHT_SIDE|on the right||||
POSITION|S_POSITION_ROADWAY|on the roadway||||
POSITION|S_POSITION_SHOULDER|on the paved shoulder||||
POSITION|S_POSITION_SLOW_LANE|in the slow vehicle lane||||
POSITION|S_POSITION_THROUGH_LANE|in the through traffic lane||||
POSITION|S_POSITION_TURNING_LANE|in the turning lane||||
QUALIFIER|(null)||||0.8|
QUALIFIER|S_QUALIFIER_ACCESS_ONLY|access only|||Deprecated|Use S_TRAFFIC_ACCESS_ONLY instead
QUALIFIER|S_QUALIFIER_BORDER_ENTRY|on entry into the country|||0.8|
QUALIFIER|S_QUALIFIER_BORDER_EXIT|on leaving the country|||0.8|
QUALIFIER|S_QUALIFIER_DAYTIME|during the day time||||
QUALIFIER|S_QUALIFIER_EXCEPT|except||||
QUALIFIER|S_QUALIFIER_FAIRLY_FREQUENT_SERVICE|fairly frequent service||||
QUALIFIER|S_QUALIFIER_FOR_ARRIVALS|for arrivals|||Deprecated|Use S_TRAFFIC_ARRIVALS instead
QUALIFIER|S_QUALIFIER_FOR_DEPARTURES|for departures|||Deprecated|Use S_TRAFFIC_DEPARTURES instead
QUALIFIER|S_QUALIFIER_FOR_HOLIDAY_TRAFFIC|for holiday traffic|||Deprecated|Use S_TRAFFIC_HOLIDAY instead
QUALIFIER|S_QUALIFIER_FOR_LIMITED_TIME|for a limited time||||
QUALIFIER|S_QUALIFIER_FOR_LOCAL_TRAFFIC|for local traffic|||Deprecated|Use S_TRAFFIC_LOCAL instead
QUALIFIER|S_QUALIFIER_FOR_LONG_DISTANCE_TRAFFIC|for long distance traffic|||Deprecated|Use S_TRAFFIC_LONG_DISTANCE instead
QUALIFIER|S_QUALIFIER_FOR_REGIONAL_TRAFFIC|for regional traffic|||Deprecated|Use S_TRAFFIC_REGIONAL instead
QUALIFIER|S_QUALIFIER_FOR_RESIDENTS|for residents|||Deprecated|Use S_TRAFFIC_RESIDENTS instead
QUALIFIER|S_QUALIFIER_FOR_VISITORS|for visitors|||Deprecated|Use S_TRAFFIC_VISITORS instead
QUALIFIER|S_QUALIFIER_FREQUENT_SERVICE|frequent service||||
QUALIFIER|S_QUALIFIER_NIGHT|during the night||||
QUALIFIER|S_QUALIFIER_OFF_PEAK|during off-peak periods||||
QUALIFIER|S_QUALIFIER_ONLY|only||||
QUALIFIER|S_QUALIFIER_REGULAR_SERVICE|regular service||||
QUALIFIER|S_QUALIFIER_SEVERAL_TIMES|several times||||
QUALIFIER|S_QUALIFIER_UNCONFIRMED_REPORT|unconfirmed report||||
QUALIFIER|S_QUALIFIER_UNTIL_FURTHER_NOTICE|until further notice|||0.8|
QUALIFIER|S_QUALIFIER_VERY_FREQUENT_SERVICE|very frequent service||||
REASON|(null)||||0.8|
REASON|S_REASON_FALLING_ICE|due to falling ice||||
REASON|S_REASON_HEAT|due to heat||||
REASON|S_REASON_HOLIDAY_TRAFFIC|due to holiday traffic|||0.8|
REASON|S_REASON_TECHNICAL_PROBLEMS|due to technical problems|||0.8|
REASON|S_REASON_TRAFFIC|due to intense traffic|||0.8|
REASON|S_REASON_VISITOR_NUMBER|due to large numbers of visitors|||0.8|
SPEED|(null)||||0.8|
SPEED|S_SPEED_CHECKS|police speed checks in operation|||0.8|
SPEED|S_SPEED_HEAVY_VEHICLE_LIMIT|speed limit in force for heavy vehicles||||
SPEED|S_SPEED_OBSERVE_LIMITS|observe speed limits|||0.8|
SPEED|S_SPEED_OBSERVE_RECOMMENDATION|observe recommended speed||||
SPEED|S_SPEED_REDUCE|reduce your speed|||0.8|
SUGGESTION|(null)||||Reserved|
SUGGESTION|S_SUGGESTION_AVOID_RUSH_HOUR|avoid the rush hour||||
SUGGESTION|S_SUGGESTION_NO_RIDE_DAY|is this your no-ride day?||||
SUGGESTION|S_SUGGESTION_P_AND_R|why not park-and-ride?||||
SUGGESTION|S_SUGGESTION_PARKING_COVERS_RIDE|your parking ticket covers the return ride||||
SUGGESTION|S_SUGGESTION_PUBLIC_TRANSPORT|why not try public transport?||||
SUGGESTION|S_SUGGESTION_RIDE_SHARE|why not ride share?||||
TENDENCY|(null)||||0.1|
TENDENCY|S_TENDENCY_QUEUE_DECREASING|traffic queue length decreasing|traffic queue length decreasing at an average rate of $Q|q_speed|0.1|
TENDENCY|S_TENDENCY_QUEUE_INCREASING|traffic queue length increasing|traffic queue length increasing at an average rate of $Q|q_speed|0.1|
TRAFFIC|(null)||||0.8|
TRAFFIC|S_TRAFFIC_ACCESS_ONLY|access only|||0.8|
TRAFFIC|S_TRAFFIC_ARRIVALS|for arrivals|||0.8|
TRAFFIC|S_TRAFFIC_DEPARTURES|for departures|||0.8|
TRAFFIC|S_TRAFFIC_FERRY|for ferry service|||0.8|
TRAFFIC|S_TRAFFIC_HOLIDAY|for holiday traffic|||0.8|
TRAFFIC|S_TRAFFIC_LOCAL|for local traffic|||0.8|
TRAFFIC|S_TRAFFIC_LONG_DISTANCE|for long distance traffic|||0.8|
TRAFFIC|S_TRAFFIC_RAIL|for rail services|||0.8|
TRAFFIC|S_TRAFFIC_REGIONAL|for regional traffic|||0.8|
TRAFFIC|S_TRAFFIC_RESIDENTS|for residents|||0.8|
TRAFFIC|S_TRAFFIC_THROUGH|for through traffic|||0.8|
TRAFFIC|S_TRAFFIC_VISITORS|for visitors|||0.8|
VEHICLE|(null)||||0.1|
VEHICLE|S_VEHICLE_4WD_WITH_SNOW_TIRES_OR_CHAINS|for 4-wheel-drive with snow tires or chains only|||Reserved|
VEHICLE|S_VEHICLE_ABNORMAL_LOAD|for abnormal loads only|||Reserved|
VEHICLE|S_VEHICLE_ALL|for all vehicles|||0.1|
VEHICLE|S_VEHICLE_ARTICULATED|for articulated vehicles only|||Reserved|
VEHICLE|S_VEHICLE_BUS|for buses only|||0.1|
VEHICLE|S_VEHICLE_CAR|for cars only|||0.1|
VEHICLE|S_VEHICLE_CAR_AND_LIGHT|for cars and light vehicles only|||Reserved|
VEHICLE|S_VEHICLE_CAR_WITH_CARAVAN|for cars with caravans only|||0.1|
VEHICLE|S_VEHICLE_CAR_WITH_TRAILER|for cars with trailers only|||0.1|
VEHICLE|S_VEHICLE_DIESEL|for diesel-engined vehicles only|||Reserved|
VEHICLE|S_VEHICLE_EVEN_PLATE|with even-numbered registration plates|||Reserved|
VEHICLE|S_VEHICLE_EXCEPTIONAL_LOAD|for exceptional loads only|||Reserved|
VEHICLE|S_VEHICLE_FERRY|for ferry service|||Deprecated|Use S_TRAFFIC_FERRY instead
VEHICLE|S_VEHICLE_GASOLINE|for petrol-engined vehicles only|||Reserved|
VEHICLE|S_VEHICLE_HAZMAT|for hazardous loads only|||0.1|
VEHICLE|S_VEHICLE_HEAVY|for heavy vehicles only|for heavy vehicles over $Q only|q_weight|0.8|
VEHICLE|S_VEHICLE_HGV|for heavy lorries only|||0.1|
VEHICLE|S_VEHICLE_HIGH_SIDED|for high-sided vehicles only|||Reserved|
VEHICLE|S_VEHICLE_LIGHT|for light vehicles only|for light vehicles below $Q only|q_weight|Reserved|
VEHICLE|S_VEHICLE_LPG|for LPG vehicles only|||Reserved|
VEHICLE|S_VEHICLE_MOTOR|for all motor vehicles|||0.1|
VEHICLE|S_VEHICLE_ODD_PLATE|with odd-numbered registration plates|||Reserved|
VEHICLE|S_VEHICLE_RAIL|for rail services|||Deprecated|Use S_TRAFFIC_RAIL instead
VEHICLE|S_VEHICLE_THROUGH_TRAFFIC|for through traffic|||Deprecated|Use S_TRAFFIC_THROUGH instead
VEHICLE|S_VEHICLE_UNDERGROUND|on the underground|||Reserved|
VEHICLE|S_VEHICLE_WITH_CAT|for vehicles with catalytic converters|||Reserved|
VEHICLE|S_VEHICLE_WITH_TRAILER|for vehicles with trailers only|||0.1|
VEHICLE|S_VEHICLE_WITHOUT_CAT|for vehicles without catalytic converters|||Reserved|
WARNING|(null)||||0.8|
WARNING|S_WARNING_ACCIDENT_RISK|increased risk of accident||||
WARNING|S_WARNING_ACCIDENTS|several accidents have taken place||||
WARNING|S_WARNING_DANGER|danger||||
WARNING|S_WARNING_EMERGENCY_VEHICLES|emergency vehicles at scene||||
WARNING|S_WARNING_EXPLOSION_HAZARD|danger of explosion||||
WARNING|S_WARNING_EXTRA_POLICE_PATROLS|extra police patrols in operation||||
WARNING|S_WARNING_FIRE_HAZARD|danger of fire||||
WARNING|S_WARNING_HELICOPTER_RESCUE|helicopter rescue in progress||||
WARNING|S_WARNING_OBSERVE_FLAGMAN|look out for flagman||||
WARNING|S_WARNING_PILOT_CAR|pilot car in operation||||
WARNING|S_WARNING_POLICE_IN_ATTENDANCE|police in attendance||||
WARNING|S_WARNING_RADIATION|radiation hazard|||0.8|
WARNING|S_WARNING_REPAIR|repairs in progress||||
WARNING|S_WARNING_RESCUE_WORK|rescue and recovery work in progress||||
WARNING|S_WARNING_TOXIC_LEAK|toxic leak|||0.8|
WARNING|S_WARNING_TRAFFIC_DIRECTED_AROUND_ACCIDENT|traffic being directed around accident area||||
WINTER|(null)||||0.8|
WINTER|S_WINTER_CHAINS_MANDATORY|snow chains mandatory||||
WINTER|S_WINTER_CHAINS_OR_SNOW_TIRES_REQUIRED|snow chains or tires required||||
WINTER|S_WINTER_CHAINS_RECOMMENDED|snow chains recommended||||
WINTER|S_WINTER_CHAINS_REQUIRED|snow chains required||||
WINTER|S_WINTER_CLOSURE|winter closure|||0.8|
WINTER|S_WINTER_EQUIPMENT_RECOMMENDED|winter equipment recommended||||
WINTER|S_WINTER_SNOW_TIRES_MANDATORY|snow tires mandatory||||
WINTER|S_WINTER_SNOW_TIRES_RECOMMENDED|snow tires recommended||||
